package com.microservicio.cd;

public interface Operacion {

	int operar(int num1, int num2);
}
