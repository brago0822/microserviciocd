import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.microservicio.cd.Suma;

import junit.framework.TestCase;

class SumaTest extends TestCase{

	@Test
	void test() {
		Suma suma = new Suma();
		int resultado = suma.operar(3, 5);
		   assertEquals("5 + 3 debe ser igual a 8",8, resultado);
	}

}
