import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.microservicio.cd.Multiplicacion;
import com.microservicio.cd.Suma;

import junit.framework.TestCase;

class MultiplicacionTest extends TestCase{

	@Test
	void test() {
		Multiplicacion mult = new Multiplicacion();
		int resultado = mult.operar(3, 5);
		   assertEquals("5 * 3 debe ser igual a 15",15, resultado);
	}

}
